var app = angular.module ("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "dashboard.html"
    })
    .when("/chatApp", {
        templateUrl : "partia1/chatapp.htm1"
    })
    .when("/propertyList", {
        templateUrl : "partial/propertyList.html"
    })
    .when("/invoice", {
        templateUrl : "partial/invoice.html"
    });
    .when("/mailBox", {
        templateUrl : "partial/mailbox.html"
    });
    .when("/mailDetails", {
        templateUrl : "partial/maildetails.html"
    });
    .when("/profile", {
        templateUrl : "partial/profile.html"
    });
});